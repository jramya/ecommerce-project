from django.contrib.auth.models import User
from django.http import Http404

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rest_framework import status
from django.shortcuts import get_object_or_404

from rest_framework.renderers import JSONRenderer


from .models import Orders,Products,OrdersItems
from .serializers import UserSerializer,ProductSerializer,OrdersSerializer,Order_items_Serializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class ProductApi(APIView):
    def get(self, request,**kwargs):
        if kwargs.get('pk'):
            pk = kwargs.get('pk')
            obj = get_object_or_404(Products.objects.all(), pk=pk)
            movie = ProductSerializer(obj)
            return Response({'movie' : movie.data})

        movie = Products.objects.all()
        movies = ProductSerializer(movie, many = True)
        return Response({'movies':movies.data})

    def post(self, request):
        serializer = ProductSerializer(data = request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)

        return Response(serializer.errors, status= status.HTTP_401_BAD_CREATED)

    def put(self, request,pk):
        movie = get_object_or_404(Products.objects.all(), pk = pk)
        mv = ProductSerializer(instance = movie, data= request.data, partial = True)
        if mv.is_valid(raise_exception = True):
            mv.save()
            print(mv)
        return Response(mv.data)

    def delete(self, request, pk):
        movie = get_object_or_404(Products.objects.all(), pk=pk)
        movie.delete()
        return Response({"message":"deleed successfully"}, status = 204)


class OrderListAPi(APIView):
    authentication_classes = [TokenAuthentication,BasicAuthentication,SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, **kwargs):
        if kwargs.get('pk'):
            pk = kwargs.get('pk')
            obj = get_object_or_404(Orders.objects.all(), pk=pk)
            orders = OrdersSerializer(obj)
            return Response({'orders': orders.data})
        orders = Orders.objects.all()
        serializer = OrdersSerializer(orders, many=True).data
        return Response(serializer, status=200)

    def post(self,request):

         user = request.data.get("user")
         products = request.data.get("products")
         total = request.data.get("total")
         created_At = request.data.get("created_At")
         updated_At = request.data.get("updated_At")
         status = request.data.get("status")
         mode_of_payment = request.data.get("mode_of_payment")

         order_instance= Orders()
         order_instance.products_id=products
         order_instance.user_id = user
         order_instance.total = total
         order_instance.created_At = created_At
         order_instance.updated_At = updated_At
         order_instance.status = status
         order_instance.mode_of_payment = mode_of_payment

         order_instance.save()

         return Response(('Order Successfully Created'))


    def put(self, request,pk):
        order = get_object_or_404(Orders.objects.all(), pk = pk)
        mv = OrdersSerializer(instance = order, data= request.data, partial = True)
        if mv.is_valid(raise_exception = True):
            mv.save()
            print(mv)
        return Response(mv.data)

    def delete(self, request, pk):
        movie = get_object_or_404(Orders.objects.all(), pk=pk)
        movie.delete()
        return Response({"message":"deleed successfully"}, status = 204)



class OrderItemsAPi(APIView):
    authentication_classes = [TokenAuthentication,BasicAuthentication,SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, **kwargs):
        if kwargs.get('pk'):
            pk = kwargs.get('pk')
            obj = get_object_or_404(OrdersItems.objects.all(), pk=pk)
            orders = Order_items_Serializer(obj)
            return Response({'orderitems': orders.data})
        orders = OrdersItems.objects.all()
        serializer = Order_items_Serializer(orders, many=True).data
        return Response(serializer, status=200)

    def post(self,request):

         products = request.data.get("products")
         order = request.data.get("order")
         Quantity = request.data.get("Quantity")


         order_instance= OrdersItems()
         order_instance.products_id=products
         order_instance.orders_id = order
         order_instance.Quantity = Quantity

         order_instance.save()

         return Response(('OrderItems Successfully Created'))


    def put(self, request,pk):
        order = get_object_or_404(OrdersItems.objects.all(), pk = pk)
        mv = Order_items_Serializer(instance = order, data= request.data, partial = True)
        if mv.is_valid(raise_exception = True):
            mv.save()
            print(mv)
        return Response(mv.data)

    def delete(self, request, pk):
        movie = get_object_or_404(OrdersItems.objects.all(), pk=pk)
        movie.delete()
        return Response({"message":"deleed successfully"}, status = 204)





